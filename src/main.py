'''
Created on 25/feb/2015

@author: Davide Guastella <davide.guastella90@gmail.com>

Copyright (C) 2015 Davide Guastella <davide.guastella90@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License, version 3
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program.  If not, see
<http://www.gnu.org/licenses/>.

'''

import gi
import os
from CustomGtkImage import CustomGtkImage
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk,GdkPixbuf,Gio,Gdk

class main_window:
    glade_file  = "../glade/main_window.glade"
    source_path = os.path.dirname(os.path.abspath(__file__))
    
    #The name of the current showed image
    current_image_name = ""
    
    #A dictionary that stores the paths of the loaded images
    im_fnames   = {}
    
    #stores the zoom height level of every loaded image
    images_zoom_height = {}
    
    #stores the zoom width level of every loaded image
    images_zoom_width = {}
    
    #the current image zoom level
    current_image_zoom_level = 1;
    
    #the zoom factor used to scale images
    ZOOM_FACTOR = 0.8;
    
    
    
    def __init__(self):
        self.glade = Gtk.Builder()
        self.glade.add_from_file(self.source_path+'/'+main_window.glade_file)
        
        #Get the widget from glade
        self.window             = self.glade.get_object("main_window")
        self.viewport_image     = self.glade.get_object("viewport")
        self.image              = CustomGtkImage()
        self.viewport_thumbnail = self.glade.get_object("viewport_thumbnail")
        self.icon_view_box      = self.glade.get_object("icon_view_box")
        self.thumbnail_box      = self.glade.get_object("thumbnail_box")
        
        self.window.connect('delete-event', Gtk.main_quit)
        
        #add the custom image widget into the viewport
        self.viewport_image.add(self.image)
        
        #Set the initial properties for the window
        self.set_window_properties()

        #Set the header bar widget
        self.set_HeaderBar_widget()
        
        #set up the thumbnail widget
        self.set_thumbnail_view_widget()
        
        self.popup_menu = Gtk.Menu()
        self.delete_popup_entry = Gtk.MenuItem("Delete")
        self.delete_popup_entry.connect('activate', self.on_delete_popup_entry_activated)
        self.popup_menu.append(self.delete_popup_entry)
        
        #show the window
        self.window.show_all()
    
    '''
    Set up the thumbnail iconView widget
    '''
    def set_thumbnail_view_widget(self):
        self.model          = Gtk.ListStore(str, GdkPixbuf.Pixbuf)
        self.icon_view      = Gtk.IconView(self.model)
        self.icon_view.set_text_column(0)
        self.icon_view.set_activate_on_single_click(True)
        self.icon_view.set_pixbuf_column(1)
        self.icon_view.set_item_width(70)
        self.icon_view.connect('button-press-event',self.on_iconview_right_button_press)
        self.thumbnail_box.set_size_request(-1,140)
        self.icon_view.connect("item-activated", self.on_thumbnail_view_item_activated)
        self.icon_view_box.pack_start(self.icon_view,expand=True, fill=True, padding=0)
    
    '''
    Callback for signal "button-press-event" of icon_view widget
    '''
    def on_iconview_right_button_press(self,iconview,event):
        if event.type != Gdk.EventType.BUTTON_PRESS:
            return False
        path = iconview.get_path_at_pos(event.x, event.y)
        if path is None:
            iconview.unselect_all()

        if event.button == Gdk.BUTTON_SECONDARY:
            self.show_popup_menu(iconview, path, event)
            return True
        return False
    
    '''
    Callback for "delete" popup menu entry (showed when user right-click the thumbnail view widget)
    '''
    def on_delete_popup_entry_activated(self,menuitem):
        selection = self.icon_view.get_selected_items()
        
        try:
            #get the selected thumbnail index
            selected_thumbnail_index = selection[0]
            
            #get the image names
            images = self.model[selected_thumbnail_index]
            
            #get the current selected image file name
            image_to_delete = images[0]
            
            del self.images_zoom_height[image_to_delete]
            del self.images_zoom_width[image_to_delete]
            del self.im_fnames[image_to_delete]
            del self.model[selected_thumbnail_index]
            self.current_image_name = ""
            
            #set the image to a stock image
            self.image.set_from_stock(Gtk.STOCK_MISSING_IMAGE, Gtk.IconSize.BUTTON);
        except Exception as ex:
            pass
    
    '''
    This callback occours when user right-click on the thumbnail view widget. It shows
    a popup menu with an entry "delete" used to delete the selected image.
    '''
    def show_popup_menu(self,iconview, path, event):
        self.popup_menu.show_all()
        self.popup_menu.popup(None, None, None, None, event.button, event.time)
        
    '''
    Callback for 'item-activated' event called
    '''
    def on_thumbnail_view_item_activated(self, iconview, item):
        #get the selected item from the iconview widget
        #NOTE: item[0] is the selected item index
        images = self.model[item]
        
        #using the selected image's name in the iconview widget, retrieve the complete path to 
        #that image stored in a dictionary
        self.current_image_name = images[0]
        
        image_path      = self.im_fnames[self.current_image_name]
        self.image.image_filename = image_path
        image_pixbuf    = GdkPixbuf.Pixbuf.new_from_file(image_path)
        im_height       = self.images_zoom_height[self.current_image_name]
        im_width        = self.images_zoom_width[self.current_image_name]
        
        #scale the image to its previously selected scaling
        scaled_pixbuf   = image_pixbuf.scale_simple(im_width, im_height, GdkPixbuf.InterpType.BILINEAR)
        
        #load the image
        self.image.set_from_pixbuf(scaled_pixbuf)
    
    '''
    Set various properties for this window
    '''
    def set_window_properties(self):
        self.window.set_border_width(10)
        self.window.set_default_size(400, 600)
	self.window.set_icon_from_file("image_transformer_logo.png")


    def set_HeaderBar_widget(self):
        #create a new HeaderBar widget
        hb = Gtk.HeaderBar()
        hb.set_show_close_button(True)
        hb.props.title = "Image transformer"
        
        #add a new button (Load button)
        button = Gtk.Button("Load")
        
        button.connect("clicked", self.on_load_image_button_clicked)
        hb.pack_start(button);
        
        #add a new button (Load button)
        button = Gtk.Button("Transform Image")
        #button.connect("clicked", self.on_load_image_button_clicked)
        hb.pack_start(button);
        
        #GtkMenuButton
        self.set_window_menuButton(hb)
        
    
    '''
    Add a new menuButton widget within the window's header bar
    '''
    def set_window_menuButton(self, HeaderBar):
        menumodel = Gio.Menu()
        
        #append the menu entries
        menumodel.append("Zoom in","win.zoom_in")
        menumodel.append("Zoom out","win.zoom_out")
        menumodel.append("Fit size","win.fit_size")
        menumodel.append("Original size","win.original_size")
        menumodel.append("About", "win.about")
        
        #Zoom in action
        zoom_in_action = Gio.SimpleAction.new("zoom_in",None)
        zoom_in_action.connect("activate",self.zoom_in_callback)
        
        #Zoom out action
        zoom_out_action = Gio.SimpleAction.new("zoom_out",None)
        zoom_out_action.connect("activate",self.zoom_out_callback)
        
        #Fit window action
        fit_to_window_action = Gio.SimpleAction.new("fit_size",None)
        fit_to_window_action.connect("activate",self.fit_to_window_callback)
        
        #About action
        about_action = Gio.SimpleAction.new("about", None)
        about_action.connect("activate", self.about_callback)
        
        #zoom to window action
        fit_size_action = Gio.SimpleAction.new("original_size",None)
        fit_size_action.connect("activate",self.set_original_image_size_callback)
        
        
        #add all the action to the menu model
        self.window.add_action(about_action)
        self.window.add_action(zoom_in_action)
        self.window.add_action(zoom_out_action)
        self.window.add_action(fit_to_window_action)
        self.window.add_action(fit_size_action)
        
        #create the menu button wdiget
        menubutton = Gtk.MenuButton()
        menubutton.set_menu_model(menumodel)
        
        #pack the menu button to the end of the header bar
        HeaderBar.pack_end(menubutton)
        
        #add the header bar widget to this window
        self.window.set_titlebar(HeaderBar)
    
    
    
    def zoom_in_callback(self, action, parameter):
        self.image.pixbuf_zoom_in()
        
        #store the new image size
        self.images_zoom_height[self.current_image_name]    = self.image.get_height()
        self.images_zoom_width[self.current_image_name]     = self.image.get_width()
        
        
    def zoom_out_callback(self, action, parameter):
        self.image.pixbuf_zoom_out()
        
        #store the new image size
        self.images_zoom_height[self.current_image_name]    = self.image.get_height()
        self.images_zoom_width[self.current_image_name]     = self.image.get_width()
        
    
    '''
    Scale the image to fit the available image area
    '''
    def fit_to_window_callback(self, action, parameter):
        
        viewport_rectangle = self.viewport_image.get_allocation()
        viewport_width  = viewport_rectangle.width
        viewport_height = viewport_rectangle.height
            
        self.image.set_pixbuf_size(viewport_width, viewport_height)
        
        self.images_zoom_height[self.current_image_name]    = self.image.get_height()
        self.images_zoom_width[self.current_image_name]     = self.image.get_width()
            
    '''
    Set the image to its original size
    '''
    def set_original_image_size_callback(self, action, parameter):
        self.image.restore_pixbuf_original_size()
        self.images_zoom_height[self.current_image_name]    = self.image.get_height()
        self.images_zoom_width[self.current_image_name]     = self.image.get_width()
          
    '''
    Callback for "about"
    '''
    def about_callback(self, action, parameter):
        print "You clicked \"About\""
        
        
    def on_load_image_button_clicked(self, button):
        dialog = Gtk.FileChooserDialog("Please choose a file", self.window, Gtk.FileChooserAction.OPEN, (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,Gtk.STOCK_OPEN, Gtk.ResponseType.OK))
        
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            print("Open clicked")
            print("File selected: " + dialog.get_filename())
            
            self.image.set_from_file(dialog.get_filename())
            
            self.add_image_to_thumbnail_list(dialog.get_filename())
            
            #add a new entry to the image fname dictionary
            imName = os.path.basename(dialog.get_filename())
           
            self.im_fnames[imName] = dialog.get_filename();
            self.image.image_filename = self.im_fnames[imName]
            
            #set the initial zoom level of the image
            self.images_zoom_height[imName] = self.image.get_pixbuf().get_height();
            self.images_zoom_width[imName]  = self.image.get_pixbuf().get_width();
            
            #set the current viewed image name
            self.current_image_name = imName  
            
        elif response == Gtk.ResponseType.CANCEL:
            print("Cancel clicked")
        
        dialog.destroy()
    
    '''
    Given the complete path of an image, add it to the thumbnail list
    '''
    def add_image_to_thumbnail_list(self, image_filename):
        try:
            image = GdkPixbuf.Pixbuf.new_from_file(image_filename)
            image_width     = image.get_width()
            image_height    = image.get_height()
            new_h           = (image_height * 60) / image_width   # Calculate the scaled height before resizing image
            scaled_image    = image.scale_simple(60, new_h, GdkPixbuf.InterpType.BILINEAR)
            imName          = os.path.basename(image_filename)
            self.model.append((imName, scaled_image))
            #print "Appended new image to model (",str(self.model),")"
        except Exception as inst:
            print type(inst)
            print inst
            print("Error adding image to thumbnail list")
            pass
        
        
    
    
if __name__ == '__main__':
    imageTransformer = main_window()
    Gtk.main()
    
