'''
Created on 22/mar/2015

@author: Davide Guastella <davide.guastella90@gmail.com>

Copyright (C) 2015 Davide Guastella <davide.guastella90@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License, version 3
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program.  If not, see
<http://www.gnu.org/licenses/>.

'''

import gi
import pygtk
pygtk.require('2.0')
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk,GdkPixbuf

class CustomGtkImage(Gtk.Image):
    
    image_filename = ""
    
    #the current image zoom level
    current_image_zoom_level = 1;
    
    #the zoom factor used to scale images
    ZOOM_FACTOR = 0.8;
    
    def __init__(self):
        Gtk.Image.__init__(self)
        self.set_from_stock(Gtk.STOCK_MISSING_IMAGE, Gtk.IconSize.BUTTON);
        
        
        
    def pixbuf_zoom_in(self):
        im = self.get_pixbuf()
        if(im):
            try:
                self.current_image_zoom_level = self.current_image_zoom_level / self.ZOOM_FACTOR;
                self.rescale_image()
                
            except Exception as ex:
                print type(ex)
                print ex
                print "[CustomGtkImage] ERROR: zoom in action failed"
                pass
        
    def pixbuf_zoom_out(self):
        im = self.get_pixbuf()
        if(im):
            try:
                self.current_image_zoom_level = self.current_image_zoom_level * self.ZOOM_FACTOR;
                self.rescale_image()
            except Exception as ex:
                print type(ex)
                print ex
                print "[CustomGtkImage] ERROR: zoom out action failed"
                pass
    
    '''
    Scale the image to fit the available image area
    '''
    def set_pixbuf_size(self, new_width, new_height):
        im = self.get_pixbuf()
        if(im):
            
            try:
                image_pixbuf    = GdkPixbuf.Pixbuf.new_from_file(self.image_filename)
                scaled_pixbuf   = image_pixbuf.scale_simple(new_width, new_height, GdkPixbuf.InterpType.BILINEAR)
                self.set_from_pixbuf(scaled_pixbuf)
            except Exception as ex:
                print type(ex)
                print ex
                print "[CustomGtkImage] ERROR: fit to window action failed (",ex.ToString(),")"
                pass
    
    def rescale_image(self):
        image_pixbuf    = GdkPixbuf.Pixbuf.new_from_file(self.image_filename)
                
        #get the current image's size
        pixbuf_height   = image_pixbuf.get_height()
        pixbuf_width    = image_pixbuf.get_width()
        
        #calculate the new image's width/height
        new_height  = int(pixbuf_height * self.current_image_zoom_level)
        new_width   = int(pixbuf_width  * self.current_image_zoom_level)
        
        #scale the image
        scaled_pixbuf   = image_pixbuf.scale_simple(new_width, new_height, GdkPixbuf.InterpType.BILINEAR)
        
        #set the image into the gtk image widget
        self.set_from_pixbuf(scaled_pixbuf)
    
         
    '''
    Get the image height
    '''
    def get_height(self):
        return self.get_pixbuf().get_height()    
    
    '''
    Get the image width
    '''
    def get_width(self):
        return self.get_pixbuf().get_width()
    
    '''
    Set the image to its original size
    '''
    #def set_original_image_size_callback(self, action, parameter):
    def restore_pixbuf_original_size(self):
        im = self.get_pixbuf()
        if(im):
            try:
                image_pixbuf    = GdkPixbuf.Pixbuf.new_from_file(self.image_filename)
                
                self.set_from_pixbuf(image_pixbuf)
            except Exception as ex:
                print type(ex)
                print ex
                print "[CustomGtkImage] ERROR: set original image size action failed (",ex.ToString(),")"
                pass
